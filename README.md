### Dados SQL Server

#### Vá até a pasta include, abra o arquivo banco.inc.php, edite o arquivo inserindo os dados de conexão da sua instância SQL Server.

### Download composer

```
https://getcomposer.org/download/
```

### Instalar dependências.

```
cd path/to/project
composer install
```

<?php
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Configuration;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Silex\Provider\SwiftmailerServiceProvider;

error_reporting(E_ALL);
ini_set('display_errors', true);


date_default_timezone_set('America/Sao_Paulo');

require __DIR__ . '/vendor/autoload.php';

$app = new Application();

$app['debug'] = true;

include "include/banco.inc.php";

$app->before(function (Request $request, Application $app) {
  $token = $request->headers->get('Token');
  $method = $request->getMethod();
  $route = $request->getPathInfo();
  
  $sql = "SELECT username FROM users WHERE token like :token";
  $app['user'] = $app['db']->fetchAssoc($sql, array('token' => $token));

  }, Application::EARLY_EVENT);
  
  $app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Token, Origin, X-Requested-With, Content-Type, Accept, Authorization');
  });
  
  $app->options("{anything}", function () {
    return new \Symfony\Component\HttpFoundation\JsonResponse(null, 204);
  })->assert("anything", ".*");

  $app->post('/login', function(Request $request) use ($app){
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    
    $data = json_decode($request->getContent(), true);
    
    $username = $data['username'];
    $password = $data['password'];
    $token = md5(uniqid());
    $params = $request->request->all();
    
    $sql = 'SELECT * FROM users WHERE username = :username AND password = :password';
    $post = $app['db']->fetchAssoc($sql, array('username' => $data['username'], 'password' => $data['password']));
    
    $sql1 = "UPDATE users SET token = :token WHERE username = :username";
    $stmt = $app['db']->prepare($sql1);
    $stmt->bindValue("token", $token);
    $stmt->bindValue("username", $data['username']);
    $stmt->execute();
    
    $post['token'] = $token;
    
    return $app->json(array(
      'dados' => $post
    ),200);
  })
  ->bind('verifica-login');

  $app->post('/cadastrar-produto', function(Request $request) use ($app){
    $data = json_decode($request->getContent(), true);

    $codProduct = $data['codigo'];
    $descProduct = $data['descProduto'];
    $priceProduct = $data['precoProduto'];

    $sql = 'SELECT cod FROM produtos WHERE cod = :cod';
    $post = $app['db']->fetchAssoc($sql, array('cod' => $codProduct));

    if($post['cod'] == $codProduct){
      return $app->json(array(
        'existe' => true
      ));
    } else {
        $app['db']->insert('produtos', array(
          'cod' => $codProduct,
          'descricao' => $descProduct,
          'preco' => $priceProduct
          )
        );
      }

    return true;
  })
  ->bind('cadastrar-produto');

   $app->post('/insert-product', function(Request $request) use ($app){
    $data = json_decode($request->getContent(), true);

    $codProduct = $data['cod'];

    $sql = 'SELECT cod FROM produtos WHERE cod = :cod';
    $post = $app['db']->fetchAssoc($sql, array('cod' => $codProduct));

    $sql2 = 'SELECT * FROM produtos WHERE cod = :cod';
    $post2 = $app['db']->fetchAssoc($sql2, array('cod' => $codProduct));

    $sql3 = "DELETE FROM documento WHERE numeroVenda = :numeroVenda AND totalProdutos < :totalProdutos AND vendaBy = :vendaBy";
    $stmt = $app['db']->prepare($sql3);
    $stmt->bindValue("numeroVenda", $data['numeroVenda']);
    $stmt->bindValue("totalProdutos", $data['totalItens'] + 1);
    $stmt->bindValue("vendaBy", $data['vendaBy']);
    $stmt->execute();

    if($post['cod'] !== $codProduct){
      return $app->json(array(
        'naoExiste' => true
      ), 200);
    }
    else {
      $app['db']->insert('documento', array(
        'numeroVenda' => $data['numeroVenda'],
        'totalProdutos' => $data['totalItens'] + 1,
        'confirmado' => 'Não',
        'vendaBy' => $data['vendaBy']
        )
      );
      return $app->json($post2, 200);
    }

    return true;
  })
  ->bind('insert-product');

  $app->post('/confirmar-venda', function(Request $request) use ($app){
    $data = json_decode($request->getContent(), true);

    $sql2 = 'SELECT id FROM documento WHERE numeroVenda = :numero';
    $post2 = $app['db']->fetchAssoc($sql2, array('numero' => $data['numeroVenda']));

    foreach ($data['produtos'] as $value) {
      $sql = 'SELECT id FROM produtos WHERE cod = :cod';
      $post = $app['db']->fetchAssoc($sql, array('cod' => $value['cod']));

      foreach ($post as $value2){
        $app['db']->insert('item', array(
          'idDocumento' => $post2['id'],
          'idProduto' => $value2
          )
        );
      }
    }

    $updateConfirm = "UPDATE documento SET confirmado = 'Sim' WHERE numeroVenda = :numeroVenda";
    $stmt = $app['db']->prepare($updateConfirm);
    $stmt->bindValue("numeroVenda", $data['numeroVenda']);
    $stmt->execute();

    return true;
  })
  ->bind('confirmar-venda');

  $app->get('/total-vendido', function(Request $request) use ($app){

    $sql = "SELECT * FROM documento WHERE confirmado = 'Sim'";
    $stmt = $app['db']->prepare($sql);
    $stmt->execute();

    $totalVendido = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if($totalVendido){
      return $app->json(array(
        'totalVendido' => $totalVendido
      ), 200);
    }
    else {
      return $app->json(array(
        'semDocumento' => true
      ));
    }

    return true;
  })
  ->bind('total-vendido');

  $app->post('/cancelar-venda', function(Request $request) use ($app){
    $data = json_decode($request->getContent(), true);

    $updateCancel = "UPDATE documento SET confirmado = 'Cancelado' WHERE numeroVenda = :numeroVenda";
    $stmt = $app['db']->prepare($updateCancel);
    $stmt->bindValue("numeroVenda", $data['numeroVenda']);
    $stmt->execute();

    return true;
  })
  ->bind('cancelar-produto');
  
  $app->post('/remove-token', function(Request $request) use ($app){
    $data = json_decode($request->getContent(), true);
    
    $updateToken0 = "UPDATE users SET token = 0 WHERE token = :token";
    $stmt = $app['db']->prepare($updateToken0);
    $stmt->bindValue("token", $data['token']);
    $stmt->execute();
    return true;
  })
  ->bind('remove-token');

  $app->run();